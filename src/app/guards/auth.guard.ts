import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate  {

  constructor(private user: UserService, private router: Router) {}

  canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot){

    return new Observable<boolean>(observer => {
      this.user.getAuthToken().then( token => {
        console.log(token);
        if(!token){
          this.router.navigate(['tutorial']);
          return observer.next(false);
        }else{
          this.user.getUser().then( r => {
            console.log(r);
            if(!r){
              this.router.navigate(['tutorial']);
              return observer.next(false);
            }else{
              return observer.next(true);
            } 
          })
          
        }
      })
      
    })
      
  }
  
}
