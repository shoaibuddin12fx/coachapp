import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { OtherEventsComponent } from './other-events.component';

@NgModule({
	declarations: [
		OtherEventsComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		
	],
	exports: [
		OtherEventsComponent
	]
})
export class OtherEventsComponentsModule {}
