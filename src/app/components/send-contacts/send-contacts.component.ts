import { Component, OnInit, Injector, Input } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page';
import { ModalController, AlertController } from '@ionic/angular';
import { ContactSelectionComponent } from '../contact-selection/contact-selection.component';

@Component({
  selector: 'app-send-contacts',
  templateUrl: './send-contacts.component.html',
  styleUrls: ['./send-contacts.component.scss'],
})

export class SendContactsComponent extends BasePage implements OnInit {
   @Input() id;
   items: any[] = [];
   _items: any[] = [];

  constructor(injector: Injector, private modalCtrl: ModalController, private alertCtrl: AlertController) { 
    super(injector);
  }

  ContactToPassService : any[] = [];

  ngOnInit() {
    this.initContactToPassService();
  }
  
  async initContactToPassService() {
    this.items = await this.contacts.getContactToPassService(this.id) as [];
    this._items = [...this.items];
    
  }

  getItems(ev: any, flag) {
    const val = ev.target.value;
    if (val && val.trim() != '') {

        this.items = this._items.filter((item) => {
          return (item.added_user.fullName && item.added_user.fullName.toLowerCase().indexOf(val.toLowerCase()) > -1)
          || ( item.added_user.email && item.added_user.email.toLowerCase().indexOf(val.toLowerCase()) > -1 )
          || ( item.added_user.phone_number && item.added_user.phone_number.toLowerCase().indexOf(val.toLowerCase()) > -1 ) ;
        });
    }
  }

  

  options(item){
    console.log(item);
    this.presentPopover(null, {pid: item, flag: 'D', delete_label: 'Remove ' + item.added_user.fullName }).then( v => {
      if (v == null || v['data'] == null) { return; }
      // console.log("remove");
      this.initremoveContactToPassService(item.id);
    })
    
  }
  async initremoveContactToPassService(id){
    // await this.contacts.removeContactToPassService(id);
  }
  async addContactToList($event){
    const modal = await this.modalCtrl.create({
      component: ContactSelectionComponent,
      componentProps: {
        
      }
    });
    modal.onDidDismiss().then( async data => {
      if( data == null || data.data == null){ return }
      let d: any = data.data;
      if(d.length > 0){
        // this.items = await this.contacts.addContactToPassService(this.id,d) as [];
        this._items = [...this.items]
      }
      
    })
    return await modal.present();
  }

  
  

  onDismiss(item){
    this.modalCtrl.dismiss(item);
  }
}
