import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-scroll-alpha',
  templateUrl: './scroll-alpha.component.html',
  styleUrls: ['./scroll-alpha.component.scss'],
})
export class ScrollAlphaComponent implements OnInit {

  alphalist: any[] = [
    {
      "alpha": "A",
      "enable": false
    },
    {
      "alpha": "B",
      "enable": false
    },
    {
      "alpha": "C",
      "enable": false
    },
    {
      "alpha": "D",
      "enable": false
    },
    {
      "alpha": "E",
      "enable": false
    },
    {
      "alpha": "F",
      "enable": false
    },
    {
      "alpha": "G",
      "enable": false
    },
    {
      "alpha": "H",
      "enable": false
    },
    {
      "alpha": "I",
      "enable": false
    },
    {
      "alpha": "J",
      "enable": false
    },
    {
      "alpha": "K",
      "enable": false
    },
    {
      "alpha": "L",
      "enable": false
    },
    {
      "alpha": "M",
      "enable": false
    },
    {
      "alpha": "N",
      "enable": false
    },
    {
      "alpha": "O",
      "enable": false
    },
    {
      "alpha": "P",
      "enable": false
    },
    {
      "alpha": "Q",
      "enable": false
    },
    {
      "alpha": "R",
      "enable": false
    },
    {
      "alpha": "S",
      "enable": false
    },
    {
      "alpha": "T",
      "enable": false
    },
    {
      "alpha": "U",
      "enable": false
    },
    {
      "alpha": "V",
      "enable": false
    },
    {
      "alpha": "W",
      "enable": false
    },
    {
      "alpha": "X",
      "enable": false
    },
    {
      "alpha": "Y",
      "enable": false
    },
    {
      "alpha": "Z",
      "enable": false
    }
  ];

  @Output('zalpha') zalpha: EventEmitter<any> = new EventEmitter<any>();

  constructor(public events: Events) {
    events.subscribe('filteralpha:clear', this.clearFilters.bind(this));
  }

  ngOnInit() {

  }

  clearFilters(){
    this.alphalist.forEach((item, index) => {
        item.enable = false
    });
  }

  filterByAlpha(a)
  {
    this.alphalist.forEach((item, index) => {
      if(item == a){
        a.enable = !a.enable;
      }else{
        item.enable = false
      }
    });
    if(!a.enable){
      this.zalpha.emit('');
    }else{
      this.zalpha.emit(a);
    }
    

  }



}
