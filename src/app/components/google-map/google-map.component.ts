import { Component, OnInit, ViewChild, ElementRef, Input, Injector } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UtilityService } from 'src/app/services/utility.service';
import { BasePage } from 'src/app/pages/base-page';

declare var google;

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss'],
})
export class GoogleMapComponent extends BasePage implements OnInit {

  @Input() location = 'chicago, il';
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('searchbox', {read: ElementRef}) searchbar: ElementRef;
  map: any;
  start = 'chicago, il';
  end = 'chicago, il';
  // tslint:disable-next-line:new-parens
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  myLatLng: any;
  myAddress: any;
  mmarker: any;
  paddress: string;

  searchQuery: string = '';
  items: string[];
  newAddress = false;

  constructor(injector: Injector, private modalCtrl: ModalController) { 
    super(injector);
  }

  ngOnInit() {
    this.getCoordsForGeoAddress(this.location)
  }

  initMap(localatlong) {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 13,
      center: localatlong
    });

    this.mmarker = new google.maps.Marker({
      position: localatlong,
      map: this.map,
      draggable:true,
      title: 'Hello World!'
    });

    var self = this;




    this.directionsDisplay.setMap(this.map);

    google.maps.event.addListener(this.mmarker,'dragend',function(event) {
      var lt = event.latLng.lat();
      var lg = event.latLng.lng();
      var coords = {lat: lt, lng: lg};

      self.getGeoAddress(coords)

    });

    var searchInput = this.searchbar.nativeElement.querySelector('.searchbar-input');
    console.log("Search input", searchInput);
    var searchBox = new google.maps.places.SearchBox(searchInput);
    // this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchInput);


    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      self.mmarker.setMap(null);
      // markers.forEach(function(marker) {
      //   marker.setMap(null);
      // });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
     
        // Create a marker for each place.
        self.mmarker = new google.maps.Marker({
          position: place.geometry.location,
          map: self.map,
          draggable:true,
          title: 'Hello World!'
        });

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      self.map.fitBounds(bounds);
    });



  }

  getCoordsForGeoAddress(address){

    this.utility.getCoordsForGeoAddress(address).then( coords => {
      this.initMap(coords);
    }, err => {
      if(this.newAddress ==  true){
        this.utility.getCoordsViaHTML5Navigator().then( coords => {
          console.log(coords);
          this.initMap(coords);
        }, err => {
          console.log(err);
          this.onSelectEvent(null);
        })
      }else{
        this.onSelectEvent(null);
      }

    })

  }

  getGeoAddress(coords){
    var self = this;
    var geocoder = new google.maps.Geocoder;
    var latlng = coords;
        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {

              self.paddress = results[0].formatted_address;
              self.utility.presentToast(self.paddress);
            } else {
              self.utility.presentToast('No results found');
            }
          } else {
            self.utility.presentToast('Geocoder failed due to: ' + status);
          }
        });

  }

  getMarkerLocation(){


    var lt = this.mmarker.position.lat();
    var lg = this.mmarker.position.lng();
    var coords = {lat: lt, lng: lg};


    var self = this;
    var geocoder = new google.maps.Geocoder;
    var latlng = coords;
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK') {
        console.log(results);
        if (results[0]) {

          self.paddress = results[0].formatted_address;
          self.utility.presentToast(self.paddress);

          if(self.newAddress == true){
            var _addressComponets = results[0].address_components;
            console.log(_addressComponets)
            var countryObject = _addressComponets.filter( x => x.types.includes("country"))[0];
            var country = ( countryObject ) ? countryObject['long_name'] : ""
            var stateObject = _addressComponets.filter( x => x.types.includes("administrative_area_level_1"))[0];
            var state = ( stateObject ) ? stateObject['long_name'] : ""
            var cityObject = _addressComponets.filter( x => x.types.includes("administrative_area_level_2"))[0];
            var city = ( cityObject ) ? cityObject['long_name'] : ""
            var streetObject = _addressComponets.filter( x => x.types.includes("route"))[0];
            var street = ( streetObject ) ? streetObject['long_name'] : ""

            console.log(country);


            var threepartAddress = {
              "country" : country,
              "state" : state,
              "city"  : city,
              "street" : street
            }
            var coords2 = {lat: lt, lng: lg, address: self.paddress, parts: threepartAddress};
            self.onSelectEvent(coords2);
          }else{

            var coords3 = {lat: lt, lng: lg, address: self.paddress};
            self.onSelectEvent(coords3);

          }



        } else {
          self.utility.presentToast('No results found');
        }
      } else {
        self.utility.presentToast('Geocoder failed due to: ' + status);
      }
    });


  }

  onSelectEvent(item){
    this.modalCtrl.dismiss(item);
  }

}
