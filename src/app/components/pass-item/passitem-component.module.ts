import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { PassItemComponent } from './pass-item.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
	declarations: [
		PassItemComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		NgxQRCodeModule
		
	],
	exports: [
		PassItemComponent
	]
})
export class PassItemComponentsModule {}
