import { CanGoBackService } from './../../services/can-go-back.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-custom-header',
  templateUrl: './custom-header.component.html',
  styleUrls: ['./custom-header.component.scss'],
})
export class CustomHeaderComponent implements OnInit {

  @Input() title = '';
  @Input() rightBtn = false;
  @Input() rightBtnIcon = 'more';
  @Input() cangoback = false;
  @Input() fromModal = false;
  @Output('rightBtnAction') rightBtnAction: EventEmitter<any> = new EventEmitter<any>();
  @Output('closeModal') closeModal: EventEmitter<any> = new EventEmitter<any>();
  @Input() IsTransparent = false;

  constructor(
    public nav: NavController,
    private router: Router,
  ) { 
    
  }

  ngOnInit(): void {
    
  }

  goback(){
    if(this.fromModal){
      this.closeModal.emit(true);
    }else{
      this.nav.pop();
    }
  }

}
