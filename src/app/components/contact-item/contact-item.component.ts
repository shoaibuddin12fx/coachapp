import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss'],
})
export class ContactItemComponent implements OnInit {

  @Input() id;
  @Input() image = null;
  @Input() name;
  @Input() phone;
  @Input() text;
  @Input() icon;
  @Input() isCheck = false;

  @Output('fireOptions') fireOptions: EventEmitter<any> = new EventEmitter<any>();
  
  constructor(public utility: UtilityService) { }

  ngOnInit() {}

  option($event){
    this.fireOptions.emit($event);
  }

  contactSelected($event){
    console.log($event);
  }

}
