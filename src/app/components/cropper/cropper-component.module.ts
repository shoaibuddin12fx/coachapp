
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CropperComponent } from './cropper.component';
import { NgxCropperjsModule } from 'ngx-cropperjs';

@NgModule({
	declarations: [
		CropperComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		NgxCropperjsModule
		
	],
	exports: [
		CropperComponent
	]
})
export class CropperComponentsModule {}
