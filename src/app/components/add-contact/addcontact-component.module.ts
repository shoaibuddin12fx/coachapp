import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { AddContactComponent } from './add-contact.component';
import { CustomHeaderComponentsModule } from '../custom-header/custom-header-component.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CountryCodeComponent } from '../country-code/country-code.component';
import { CountryCodeComponentsModule } from '../country-code/countrycode-component.module';

@NgModule({
	declarations: [
		AddContactComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		FormsModule,
		ReactiveFormsModule,
		CustomHeaderComponentsModule,
		CountryCodeComponentsModule
		
	],
	exports: [
		AddContactComponent
	]
})
export class AddContactComponentsModule {}
