import { Component, OnInit, Input, Injector } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { CountryCodeComponent } from '../country-code/country-code.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss'],
})
export class AddContactComponent extends BasePage implements OnInit {

  @Input() user_id;
  @Input() show_relation = false;
  @Input() isFromRequestPassScreen: boolean = false;
  aForm: FormGroup;
  submitAttempt = false;
  canModify = false;
  user: any;
  phone_contacts: any = [];
  isUpdate: boolean = false;
  btnText: string = 'Create';
  formattedNumber;
  has_house: boolean = false;
  temporary_duration: number = 6;
  dial_code = { name: 'United States', dial_code: '+1', code: 'US', image: 'assets/imgs/flags/us.png' };

  constructor(injector: Injector, public modalCtrl: ModalController, public formBuilder: FormBuilder, private http: HttpClient,

    ) {
    super(injector);
    this.user = this.userService.User;
    this.setupForm();
    
  }

  ngOnInit() {}

  setupForm(){

    this.aForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */ ],
      email: ['', Validators.compose([Validators.email,Validators.required])],
      relationship: ['none', Validators.compose([Validators.required])],
      phone_number:  [{value: '', disabled: this.isUpdate == true}, Validators.compose([Validators.required])],
      can_manage_family: [false, Validators.compose([Validators.required])],
      can_send_passes: [false, Validators.compose([Validators.required])],
      allow_parental_control: [false, Validators.compose([Validators.required])],
      is_temporary: [false, Validators.compose([Validators.required])],
      temporary_duration: ['6'],
      dial_code: ['+1'],
    });
    
    this.setparamsInView();

  }

  setparamsInView(){

    if(this.isFromRequestPassScreen == true){
      this.btnText = 'Request';
    }

    if(this.user_id){
      this.isUpdate = true;
      this.btnText = 'Update';
      this.getContactInfo(this.user_id);
    }else{

      this.canModify = true;
      var pn = this.getQueryParams().phone_number;
      var ne = this.getQueryParams().name;
      var co = this.getQueryParams().dial_code;

      if(ne){
        this.aForm.controls.name.setValue(ne);
      }

      if(pn){
        this.aForm.controls.phone_number.setValue(pn);
      }

      if(co){
        this.aForm.controls.dial_code.setValue(pn);
      }

    }
    
  }

  getContactInfo(id){

    this.contacts.getContactInfo(id).then( profile => {
      this.aForm.controls.name.setValue(profile['display_name']);
      this.aForm.controls.email.setValue(profile['email']);
      profile['relationship'] = (profile['relationship'] != null) ? profile['relationship'] : 'none';
      this.aForm.controls.relationship.setValue(profile['relationship']);
      var _tel = this.utility.onkeyupFormatPhoneNumberRuntime(profile['its_user']['phone_number']);
      this.aForm.controls.phone_number.setValue(_tel);
      this.aForm.controls.can_manage_family.setValue( ( profile['its_user']['can_manage_family'] == '1' ? true : false ) );
      this.aForm.controls.can_send_passes.setValue( ( profile['its_user']['can_send_passes'] == '1' ? true : false ) );
      this.aForm.controls.allow_parental_control.setValue( ( profile['its_user']['allow_parental_control'] == '1' ? true : false ) );
      this.canModify = profile['modify'];
      this.aForm.controls.dial_code.setValue(profile['dial_code']);
      this.setDialCode(profile['dial_code'])
    })
            
  }

  setDialCode(code){

    this.http.get('assets/countries.json').subscribe(data => {
      var n: any = data
      this.dial_code = n.find( x => x.dial_code == code )
      this.dial_code['image'] = 'assets/imgs/flags/' + this.dial_code['code'].toLowerCase() + '.png';
    })

  }

  showRelationshipOptions(){
    this.presentPopover(null, {pid: this.user_id, flag: 'RL'}).then( v => {
      this.aForm.controls.relationship.setValue(v);
    })
  }

  async create(){

    // add validations
    console.log("E");
    var in_name = !this.aForm.controls.name.valid || !this.utility.isLastNameExist(this.aForm.controls.name.value)
    var in_email = !this.aForm.controls.email.valid

    var in_phone = !this.aForm.controls.phone_number.valid
    var _validPhoneNumber = this.utility.getOnlyDigits(this.aForm.controls.phone_number.value);
    console.log(_validPhoneNumber);
    if(_validPhoneNumber.toString().length < 10){
      in_phone = true;
    }else{
      in_phone = false;
    }

    if(in_name){ this.utility.presentFailureToast( "Please Enter Full Name"); return }

    if(in_phone){ this.utility.presentFailureToast( "Please Enter Valid Phone Number"); return }
    if(in_email && this.show_relation){ this.utility.presentFailureToast( "Please Enter Email"); return }


    if(this.isFromRequestPassScreen == true){
      // check if a user is already on zuul
      const canSendPasses = await this.contacts.canUserSentPasses(_validPhoneNumber)

    }else{
      this.createFamilyMember();
    }




  }

  async createFamilyMember(){

    this.submitAttempt = true;
    var formdata = this.aForm.value;
    formdata['is_update'] = this.isUpdate;
    if(this.isUpdate){
      formdata['user_id'] = this.user_id;
    }
    formdata['is_family'] = this.show_relation;
    await this.contacts.createFamilyMember(formdata);
    this.close(true);
    

    


  }

  onTelephoneChange(ev) {
    if (ev.inputType != 'deleteContentBackward') {
      const formattedString = this.utility.onkeyupFormatPhoneNumberRuntime(ev.target.value);
      this.aForm.controls['phone_number'].setValue(formattedString);
    }
  }

  getCountryCode() {
    this.openCountryModal();
  }

  async openCountryModal() {
    const modal = await this.modalCtrl.create({
      component: CountryCodeComponent,
      componentProps:{
        type : 'flags'
      }

    });
    modal.onDidDismiss().then( data => {
      console.log(data);
      this.dial_code = data['data'] as any;
      this.aForm.controls['dial_code'].setValue(this.dial_code["dial_code"]);
    })
    return await modal.present();
  }

  close(flag){
    this.modalCtrl.dismiss(flag);
  }

}
