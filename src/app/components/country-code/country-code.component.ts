import { ModalController } from '@ionic/angular';
import { Component, OnInit, Input } from '@angular/core';
import { CountryflagService } from 'src/app/services/countryflag.service';

@Component({
  selector: 'app-country-code',
  templateUrl: './country-code.component.html',
  styleUrls: ['./country-code.component.scss'],
})
export class CountryCodeComponent implements OnInit {

  countries: any[] = [];
  _countries: any[] = [];
  states: any[] = [];
  _states: any[] = [];
  @Input() type = 'flags';


  constructor(public flags: CountryflagService, private modalCtrl: ModalController) { }

  ngOnInit() {
    this.initialize(this.type);
  }

  async initialize(type) {

    console.log(type);
    if (type == 'flags') {
      await this.flags.setFlagsArray();
      this.countries = this.flags.getFlags();
      this._countries = this.flags.getFlags();
      console.log(this.countries);
    }

    if(type == 'states'){
      await this.flags.setUsStates();
      this.states = this.flags.getUsStates();
      this._states = this.flags.getUsStates();
    }

  }

  getItems(ev: any, flag) {
    const val = ev.target.value;
    if(this.type == 'flags'){
      this.countries = this._countries
      
      if (val && val.trim() != '') {
        this.countries = this.countries.filter((item) => {
          return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }
    
    if(this.type == 'states'){

      this.states = this._states
      if (val && val.trim() != '') {
        this.states = this.states.filter((item) => {
          return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }

  }

  onSelectCountry(item){
    this.modalCtrl.dismiss(item);
  }

}
