import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { PassListItemComponent } from './pass-list-item.component';
import { SendContactComponentsModule } from '../send-contacts/send-contacts.component.module';
import { SendContactsComponent } from '../send-contacts/send-contacts.component';



@NgModule({
	entryComponents: [
		SendContactsComponent
		
	],
	declarations: [
		PassListItemComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		SendContactComponentsModule
		
	],
	exports: [
		PassListItemComponent
	]
})
export class PassListItemComponentsModule {}
