import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SendContactsComponent } from '../send-contacts/send-contacts.component';

@Component({
  selector: 'app-pass-list-item',
  templateUrl: './pass-list-item.component.html',
  styleUrls: ['./pass-list-item.component.scss'],
})
export class PassListItemComponent implements OnInit {
  @Input() id = null;
  @Input() avatar = null;
  @Input() name = null;
  @Input() community = null;
  @Input() sender = null;
  @Input() discription = null;
  @Input() startFormattedDates = null;
  @Input() formatHoursToText = null;
  @Input() qrcode = null;
  @Input() cardColor = null;
  @Input() ShowButton = false;

  constructor(public modalCtrl: ModalController) { }
  
  async sendContacts(){
    console.log(this.id);
    const modal = await this.modalCtrl.create({
      component: SendContactsComponent,
      componentProps: {
        id : this.id
      }
    });
    return await modal.present();

  }
  ngOnInit() {}

  

}
