import { Component, OnInit, Injector, Input } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-contact-selection',
  templateUrl: './contact-selection.component.html',
  styleUrls: ['./contact-selection.component.scss'],
})
export class ContactSelectionComponent extends BasePage implements OnInit {

  @Input() fromContacts: boolean = true;
  phone_contacts: any[] = [];
  buffer_contacts;
  toggle: boolean = false;
  selectionIcon = 'checkbox-outline';

  constructor(injector: Injector, private modalCtrl: ModalController) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {

    // this.fromContacts = this.utility.stringToBoolean( this.getQueryParams().fromContacts );
    if (this.fromContacts == true) {

      let cn = await this.contacts.getContactsToSync() as [];
      this.buffer_contacts = cn;
      this.phone_contacts = cn;

    } else {

      let cn = await this.contacts.getTempPhoneContacts() as [];
      this.buffer_contacts = cn;
      this.phone_contacts = cn;
      console.log(this.phone_contacts);

    }

  }

  selectAnotherNumber($event, item){
    $event.stopPropagation();
    console.log(item);
  }

  getItems(ev: any, flag) {
    const val = ev.target.value;

    this.phone_contacts = this.buffer_contacts;

    if (val && val.trim() != '') {
      this.phone_contacts = this.phone_contacts.filter((item) => {
        return (item.display_name != null && item.display_name.toLowerCase().indexOf(val.toLowerCase()) > -1)
        || (item.phone_number != null && item.phone_number.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }


  }

  onSelectEvent(item) {
    this.modalCtrl.dismiss(item);
  }

  async finishSync(){
    let selected = this.phone_contacts.filter(x => x.checked == true);
    this.modalCtrl.dismiss(selected);

  }

}
