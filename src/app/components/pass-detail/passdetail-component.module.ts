import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { PassDetailComponent } from './pass-detail.component';
import { CustomHeaderComponentsModule } from './../custom-header/custom-header-component.module';

@NgModule({
	declarations: [
		PassDetailComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		NgxQRCodeModule,
		CustomHeaderComponentsModule
		
	],
	exports: [
		PassDetailComponent
	]
})
export class PassDetailComponentsModule {}
