import { Storage } from '@ionic/storage';
import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class PassesService {

  constructor(private network: NetworkService, private storage: Storage) { }

  getAddedHourDate(dataObj){
    return new Promise( resolve => {
      this.network.getAddedHourDate(dataObj).subscribe(res => {
        resolve(res['date']);
      });
    });
  }

  getUserEvents(){
    return new Promise( resolve => {
      this.network.getUserEvents().subscribe((res: any) =>{
        resolve(res['events']);
      });
    });
  }

  addUserEvent(data){
    return new Promise( resolve => {
      this.network.addEventToList(data).subscribe((res: any) => {
        resolve(true);
      }, err => resolve(null));
    })
  }

  editUserEvent(data){
    return new Promise( resolve => {
      this.network.editEventToList(data).subscribe((res: any) => {
        resolve(true);
      }, err => resolve(null));
    })
  }

  deleteUserEvent(item){
    return new Promise( resolve => {
      this.network.removeEventFromList(item['id']).subscribe((res: any) => {
        resolve(true);
      }, err => resolve(null));
    });
  }

  setEventToStorage(item){
    this.storage.set('event', JSON.stringify(item));
  }

  getEventToStorage(){
    return this.storage.get('event');
  }

  async getDefaultEvent() {
    let ev = await this.getEventToStorage();
    if (ev) {
      ev = JSON.parse(ev);
      return { event_id :  ev.id, event_name : ev.event_name };
    } else {
      return { event_id : 14, event_name: "Event Name" };
    }
  }

  createPass(formdata){
    return new Promise( resolve => {
      this.network.createNewPass(formdata).subscribe((res: any) =>{
        resolve(true)
      }, error => resolve(null));
    })
    
  }

  getActivePasses(id) {

    return new Promise((resolve) => {
      this.network.getUserReceivedPasses(id).subscribe((data: any) => {
         console.log(data);
        resolve(data['passes']);
      }, err => { resolve([]); })
    });
    
  }

  getArchivePasses(id) {
    return new Promise((resolve) => {
      this.network.getUserArchievePasses(id).subscribe((data: any) => {
        console.log(data);
        resolve(data['archived']);
      }, err => { resolve([]); })
    });
  }
  getArchiveSentPasses(id) {
    return new Promise((resolve) => {
      this.network.getUserArchievePasses(id).subscribe((data: any) => {
        console.log(data);
        resolve(data['sentPasses']);
      }, err => { resolve([]); })
    });
  }

  getSentPasses(id){
    return new Promise((resolve) => {
      this.network.getUserSentPasses(id).subscribe((data: any) => {
        console.log(data);
        resolve(data['passes']);
      }, err => { resolve([]); })
    });
  }

  getPassDetail(id){
    return new Promise( resolve => {
      this.network.getPasseDetails(id).subscribe(res =>{
        resolve(res['pass']);
      }, err => resolve(null) );
    })
  }
  
  
}
