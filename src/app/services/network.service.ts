import { UtilityService } from './utility.service';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(
    public utility: UtilityService,
    public api: ApiService
  ) { }

  // post requests -- start
  isUserExistWithPhoneNumber(data) {
    return this.httpPostResponse('is_user_exist_with_phonenumber', data, null, false );
  }

  login(data) {
    return this.httpPostResponse('login', data, null, true );
  }

  register(data) {
    return this.httpPostResponse('signup', data, null, true );
  }

  saveFcmToken(data) {
    return this.httpPostResponse('save_fcm_token', data, null, false );
  }

  forgetPassword(data) {
    return this.httpPostResponse('forgetpassword', data, null, true );
  }

  updatePassword(data) {
    return this.httpPostResponse('updatepassword', data, null, true );
  }

  checkIfDummyPhoneExist(id, data) {
    return this.httpPostResponse('check_if_dummy_phone_exist', data, id, true );
  }

  updateProfile(data) {
    return this.httpPostResponse('profile', data, null, true );
  }

  sendCodeToBecomeResident(data) {
    return this.httpPostResponse('resident_verification', data, null, true );
  }

  sendCodeToVerifyEmail(data) {
    return this.httpPostResponse('email_verification', data, null, true );
  }

  updatePassDirections(data) {
    return this.httpPostResponse('update_pass_directions', data, null, true );
  }

  removeFromFavorites(data) {
    return this.httpPostResponse('remove_contacts_from_fav', data, null, true );
  }

  deleteContactArray(data) {
    return this.httpPostResponse('delete_contact_array', data, null, false, false );
  }

  sendRequestForAPass(data) {
    return this.httpPostResponse('send_request_for_pass', data, null, true );
  }

  addToFavorites(data) {
    return this.httpPostResponse('add_contacts_to_fav', data, null, true );
  }

  addContactsToGroup(data) {
    return this.httpPostResponse('add_contacts_to_group', data, null, true );
  }

  removeFromGroup(data) {
    return this.httpPostResponse('remove_contacts_from_group', data, null, true );
  }

  addEventToList(data) {
    return this.httpPostResponse('add_event_to_list', data, null, true );
  }

  editEventToList(data) {
    return this.httpPostResponse('edit_event_to_list', data, null, true );
  }

  getAddedHourDate(data) {
    return this.httpPostResponse('get_added_hour_date', data, null, false );
  }

  createNewPass(data) {
    return this.httpPostResponse('create_new_pass', data, null, true );
  }

  createContactGroup(data) {
    return this.httpPostResponse('create_contact_group', data, null, true );
  }

  updateContactGroup(id, data) {
    return this.httpPostResponse('update_contact_group', data, id, true );
  }

  addContactWithOnlyId(id) {
    return this.httpPostResponse('add_contact_with_only_id', {}, id, true );
  }

  getSingleContact(data) {
    return this.httpPostResponse('get_single_contact', data, null, true );
  }

  createFamilyMember(data) {
    return this.httpPostResponse('create_family_member', data, null, true );
  }

  rejectPassRequest(data) {
    return this.httpPostResponse('reject_pass_request', data, null, true );
  }

  acceptPassRequest(data) {
    return this.httpPostResponse('accept_pass_request', data, null, true );
  }

  updateUserNotificationSettings(data) {
    return this.httpPostResponse('update_user_notification_settings', data, null, true );
  }

  setManageFamilyPermission(data) {
    return this.httpPostResponse('set-manage-permission-flag', data, null, true );
  }

  setSendPassesPermission(data) {
    return this.httpPostResponse('set-send-passes-flag', data, null, true );
  }

  deleteFamilyMemberArray(data) {
    return this.httpPostResponse('delete_family_member_array', data, null, true );
  }

  syncContacts(data) {
    return this.httpPostResponse('sync_contacts', data, null, true );
  }

  addContactToPass(passid, data) {
    return this.httpPostResponse('add_contact_to_pass', data, passid, true );
  }

  phoneNumberSync(data) {
    return this.httpPostResponse('phone_number_sync', data, null, true );
  }

  retractSentPass(id, data) {
    return this.httpPostResponse('retract_sent_pass', data, id, true );
  }


  checkEmailAlreadyInUse(data) {
    return this.httpPostResponse('check_email_already_in_use', data, null, true );
  }

  verifyEmailAddress(data) {
    return this.httpPostResponse('verify_email_address', data, null, true );
  }

  setParentalControlOptions(id, data) {
    return this.httpPostResponse('set_parental_control_options', data, id, true );
  }






  // post requests -- ends

  // get requests -- start
  getUser(token = null) {
    return this.httpGetResponse('user', null, false, false );
  }

  getUserReceivedPasses(id) {
    return this.httpGetResponse('get_user_received_passes', id, true );
  }

  getUserArchievePasses(id) {
    return this.httpGetResponse('get_user_archieve_passes', id, true );
  }

  getUserSentPasses(id, expired = 0) {
    return this.httpGetResponse('get_user_sent_passes', id + '?expired=' + expired, true );
  }

  getContacts() {
    return this.httpGetResponse('get_contacts', null, true );
  }

  getContactGroups() {
    return this.httpGetResponse('get_contact_groups', null, true  );
  }

  deleteContactGroup(id) {
    return this.httpGetResponse('delete_contact_group', id, true );
  }

  getUserEvents() {
    return this.httpGetResponse('get_user_events', null, true );
  }

  removeEventFromList(qrid) {
    return this.httpGetResponse('remove_event_from_list', qrid, true );
  }

  getContactGroup(id) {
    return this.httpGetResponse('get_contact_group', id, true );
  }

  getPasseDetails(id) {
    return this.httpGetResponse('get_pass_details', id, true );
  }

  checkIfCellNumberUserCanSentPass(num) {
    return this.httpGetResponse('check_if_user_num_can_sent_pass', num, true );
  }

  checkIfCellNumberUserExists(num) {
    return this.httpGetResponse('check_if_user_num_exist', num, true, true );
  }

  getUnreadAnnouncements() {
    return this.httpGetResponse('get_unread_announcements', null, true );
  }

  removeNotification(id) {
    return this.httpGetResponse('remove_notification', id, false );
  }

  getOnePageNotifications() {
    return this.httpGetResponse('get_onepage_notifications', null, true );
  }

  getSoundLocation() {
    return this.httpGetResponse('getSoundLocation', null, false, false );
  }

  getFamilyMembers() {
    return this.httpGetResponse('get_family_members', null, true );
  }

  getContactToPass(id) {
    return this.httpGetResponse('get_contact_to_pass', id, true );
  }

  removePassRecipient(id) {
    return this.httpGetResponse('remove_pass_recipient', id, true );
  }

  togglePassEnable(id, state) {
    return this.httpGetResponse('toggle_pass_enable', id + '/' + state, true );
  }

  setReadAnnouncements(id) {
    return this.httpGetResponse('set_read_announcements', id, true );
  }

  getParentalControlOptions(id) {
    return this.httpGetResponse('get_parental_control_options', id, true );
  }


  // get requests -- end




  httpGetResponse(key, id = null, showloader = false, showError = true) {

    if (showloader === true) {
      this.utility.showLoader();
    }

    const nid = (id) ? '/' + id : '';
    const url = key + nid;
    const seq = this.api.get(url);

    seq.subscribe((res: any) => {

      if (showloader === true) {
        this.utility.hideLoader();
      }

      if (res.bool !== true) {

        if (showError) {
          this.showFailure(res);
        }

      }
      
    }, err => {

      if (showloader === true) {
        this.utility.hideLoader();
      }

      if (showError) {
        this.showFailure(err);
      }


    })

    return seq;



  }

  httpPostResponse(key, data, id = null, showloader = false,  showError = true) {

    if (showloader === true) {
      this.utility.showLoader();
    }

    const nid = (id) ? '/' + id : '';
    const url = key + nid;
    const seq = this.api.post(url, data);

    seq.subscribe((res: any) => {

      if (showloader === true) {
        this.utility.hideLoader();
      }

      if (res.bool === false) {
        if (showError) {
          this.showFailure(res);
        }

      }
    }, err => {
      
      if (showloader === true) {
        this.utility.hideLoader();
      }

      if (showError) {
        this.showFailure(err);
      }

    })

    return seq;

  }

  showSuccess() {

  }

  showFailure(err) {
    console.error('ERROR', err);
    const nerror = (err.error) ? err.error.message : err.message;
    this.utility.presentFailureToast(nerror);
  }
}
