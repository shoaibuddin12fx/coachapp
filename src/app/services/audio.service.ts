import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class AudioService {
  
  audio: any;
  received: boolean = false;

  constructor(public network: NetworkService) { }

  preload(){

    this.network.getSoundLocation().subscribe( (data) => {
      
      if(data['bool'] == true){
        this.received = true;
        this.audio = new Audio();
        this.audio.src = data['sound'];
        this.audio.load();
      }
    }, err => {})
  }

  play(key: string): void {

    if(this.received == true){
      this.audio.play();
    }


  }

}
