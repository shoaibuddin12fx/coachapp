import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { UtilityService } from './utility.service';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts/ngx';
import { HttpClient } from '@angular/common/http';
import { NetworkService } from './network.service';


@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  contacts: any = [];
  groups: any = [];
  favorites: any = [];

  isContactsLoaded = false;
  isGroupLoaded = false;
  isFavoritesLoaded = false;



  public newContactAdded = false;
  public newGroupAdded = false;


  constructor(private network: NetworkService,
              private http: HttpClient,
              private phonecontacts: Contacts,
              private utility: UtilityService ,
              private alertCtrl: AlertController) { }

  getContactsData(){
    return new Promise( resolve => {

      this.network.getContacts().subscribe(async (res: any) => {
        this.contacts = res.contact_list;
        this.isContactsLoaded = true;
        resolve(this.contacts);
      }, err => {
        resolve(this.contacts);
      });

    })
  }

  getGroupsData(){

    return new Promise( resolve => {
      this.network.getContactGroups().subscribe((res: any) => {
        this.groups = res.group;
        this.isGroupLoaded = true;
        resolve(this.groups);
      }, err => { resolve(this.groups); });
    });

  }

  getFavoritesData() {

    return new Promise( resolve => {

      this.network.getContacts().subscribe(async (res: any) => {
        console.log(res);
        this.contacts = res.contact_list;
        let fc = this.contacts.filter(o => o.is_favourite == 1);
        this.favorites = fc;
        this.isFavoritesLoaded = true;
        resolve(this.favorites);
      }, err => {
        resolve(this.favorites);
      });

    })

  }

  getContacts(fetch = false){
    return new Promise( async resolve => {
      if(!this.isContactsLoaded || fetch){
        await this.getContactsData();
        resolve(this.contacts);
      }else{
        resolve(this.contacts);
      }
    });
  }

  setContacts(contacts){
    this.contacts = contacts;
  }

  getFavorites(){
    let fc = this.contacts.filter(o => o.is_favourite == 1);
    return this.favorites = fc;
  }

  setFavorites(favorites){
    this.favorites = favorites;
  }

  getGroups(fetch = false){
    return new Promise( async resolve => {
      if(!this.isGroupLoaded || fetch){
        await this.getGroupsData();
        resolve(this.groups);
      }else{
        resolve(this.groups);
      }
    });
  }

  setGroups(groups){
    this.groups = groups;
  }

  async getSelectedGroupAlert(){

    return new Promise( async resolve => {

      const groups = await this.getGroups() as [];
      console.log(groups);
      if (groups.length == 0) {
        this.utility.presentFailureToast('No Group Found');
        resolve(null);
      }

      const options = groups.map( o => {
        return { text: o['group_name'], value: o['id'] };
      })

      this.utility.presentPickerOptions('Groups', options).then(v => {
        resolve(v);
      });


  
      // let inputs = [];
      // this.groups.forEach(item => {
      //   inputs.push({
      //     type: 'radio',
      //     label: item.group_name,
      //     value: item.id,
      //     checked: false
      //   });
      // });
  
      // const alert = await this.alertCtrl.create({
      //   header: 'Select Group',
      //   inputs: inputs,
      //   buttons: [
      //     {
      //       text: 'Cancel',
      //       role: 'cancel',
      //       cssClass: 'secondary',
      //       handler: () => {
      //         console.log('Confirm Cancel');
      //         resolve(null);
      //       }
      //     }, {
      //       text: 'Ok',
      //       handler: (id) => {
      //         console.log('Confirm Ok');
      //         resolve(id);
      //       }
      //     }
      //   ]
      // })
  
      // await alert.present();
    })

  }

  addContactsToGroup(group_id, list){
    return new Promise(resolve => {
      const dataObj = {
        group_id: group_id,
        contact_list: list
      }
  
      this.network.addContactsToGroup(dataObj).subscribe(async data => {
        console.log('result', data['list']);
  
        this.contacts.forEach(item => {
          item['checked'] = false;
        });
        this.utility.presentSuccessToast(data['message']);
        await this.getGroups(true);
        resolve();
      }, err => {
        resolve();
      });
    })
    
  }

  createSelectedInContactFavouritiesByItem(items) {
    return new Promise( resolve => {
      let fi = items;
      let self = this;
      console.log(fi);
  
      if (fi.length <= 0) {
        resolve(null);

      }
  
      this.network.addToFavorites(items).subscribe(res => {
        // this.utility.presentSuccessToast(res['message']);
        fi.forEach(element => {
          const id = self.contacts.findIndex(x => x.id == element.id)
          if(id){
            self.contacts[id].is_favourite = 1;
          }
        });
        resolve(this.getFavorites());
      }, err => { resolve(this.getFavorites()); });
    })
    
  }

  deleteSelectedContact(item){
    let self = this;
    return new Promise( resolve => {
      const contactArray = {cs: [item]}
      this.network.deleteContactArray(contactArray).subscribe(async res => {
        //this.utility.presentSuccessToast(res['message']);
      }, err => {});
      
      resolve();
    })
    
  }

  deleteContactGroup(group){
    return new Promise( resolve => {
      this.network.deleteContactGroup(group.id).subscribe((res: any) => {
        resolve();
      }, err => { resolve() });
    })
    
  }

  removeFromFavorites(item){
    return new Promise( resolve => {
      this.network.removeFromFavorites([item]).subscribe(data => {
        resolve()
      }, err => { resolve() });
    })
  }

  getContactInfo(user_id){

    return new Promise( resolve => {
      let formdata = {user_id: user_id};
      this.network.getSingleContact(formdata).subscribe((res: any) => { 
        resolve(res.contact);
      }, err => {});
    })
    
  }

  canUserSentPasses(phone_number){
    return new Promise(resolve => {
      this.network.checkIfCellNumberUserCanSentPass(phone_number).subscribe( v => {
        console.log(v);
      }, err => {})
    })
  }

  createFamilyMember(formdata){
    return new Promise(resolve => {
      this.network.createFamilyMember(formdata).subscribe((res: any) =>{
        this.utility.presentSuccessToast(res.message);
        this.newContactAdded = true;
        resolve(res);
      }, err =>{
        resolve(null);
      });
    })
  }

  getPhoneContacts(){
    return new Promise( resolve => {

      this.phonecontacts.find(['displayName', 'phoneNumbers', 'emails'], { desiredFields: ['displayName', 'phoneNumbers', 'emails'], filter: '', hasPhoneNumber: true, multiple: true })
        .then(data => {

          let cont = data.map((o) => {
            let f = {}
            f['display_name'] = o.displayName;
            f['email'] = (o.emails) ? o.emails[0]['value'] : null;

            f['phone_numbers'] = [];
            f['phone_number'] = ''
            if (o.phoneNumbers) {

              //f["phone_number"] = ["4576543265", "5578563534", "6585756373"];
              if (o.phoneNumbers.length > 0) {
                f['phone_numbers'] = o.phoneNumbers
                  .map(function (i) {
                    return { value: i.value, type: i.type };
                  });

                f['phone_number'] = f['phone_numbers'][0].value;

              }

            }

            f['checked'] = false;
            f['selected'] = false;
            f['profile_image'] = null;
            return f;
          });

          cont = cont.filter(a => a['phone_numbers'].length > 0 && (a['display_name'] != null && a['display_name'] != ''))
          cont.sort((a, b) => (a['display_name'] > b['display_name']) ? 1 : ((b['display_name'] > a['display_name']) ? -1 : 0));
          resolve(cont);
        })
        .catch(err => {
          resolve([]);
        })
    })
  }

  getTempPhoneContacts(){
    return new Promise( resolve => {
      

        this.http.get('assets/cn.json').subscribe(data => {
          resolve(data)
        })
    
      
    })
  }

  syncContacts(formdata){
    return new Promise( resolve => {
      this.network.syncContacts(formdata).subscribe((response: any) =>{
        this.utility.presentSuccessToast(response['message']);
        this.newContactAdded = true;
        resolve(true);
      }, err => {
        resolve(null);
      });
    })
  }

  createNewGroup(formdata){

    return new Promise( resolve => {

      this.network.createContactGroup(formdata).subscribe((res: any) =>{
        this.newGroupAdded = true;
        resolve(true);
      }, err => { resolve(false); });
    })
    

  }

  updateOldGroup(id, formdata){
    return new Promise( resolve => {
      this.network.updateContactGroup(id, formdata ).subscribe((res: any) =>{
        this.utility.presentSuccessToast(res.message);
        this.newGroupAdded = true;
        resolve(true);
        
      }, err => {resolve(null)});
    })
    
  }

  getGroupById(id){
    return new Promise( resolve => {
      this.network.getContactGroup(id).subscribe(res => {
        resolve(res['group']);
      }, err => { resolve(null) })
    })
    
  }

  removeFromGroup(item){
    return new Promise( resolve => {
      this.network.removeFromGroup([item]).subscribe(async res => {
        this.groups = await this.getGroupsData();
        resolve(true);
      }, err => { resolve(null) });
    })
  }

  async getContactsToSync(){
    let cn = await this.getContacts() as [];
    let cont = cn.map((o) => {
      let f = {}
      f['id'] = o['id'];
      f['user_id'] = o['user_id'];
      f['display_name'] = o['display_name'];
      f['email'] = o['email'];
      f['phone_number'] = o['phone_number'];
      f['phone_numbers'] = [{value: o['phone_number'], type: '' }];
      f['checked'] = false;
      f['selected'] = false;
      f['profile_image'] = null;
      return f;
    });
    return cont;
  }

  setContactToFav(items){
    let ids = items.map(x => x.id);
    return new Promise( resolve => {
      this.network.addToFavorites(items).subscribe(res => {
        this.contacts.forEach(element => {
          if(ids.includes(element.id)){
            element.is_favourite = 1;
          }
        });
        resolve(true);
      }, err => { resolve(null) });
    })
    
  }

  addContactWithOnlyId(id){
    return new Promise( resolve => {
      this.network.addContactWithOnlyId(id).subscribe( res => {
        this.utility.presentSuccessToast(res['message'])
        resolve(true);
      }, err => {resolve(false); })
    });
  }

  getContactToPassService(id){
    return new Promise( resolve => {
      this.network.getContactToPass(id).subscribe( data => {
        console.log(data);
        resolve(data['pass'].get_qrs);
      }, err => {resolve([]); })
    });
  }
}
