import { Injectable } from '@angular/core';
import { UtilityService } from './utility.service';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Observable, from } from 'rxjs';
import { switchMap, mergeMap, catchError } from 'rxjs/operators';
import { SqliteService } from './sqlite.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private storage: Storage, public sqlite: SqliteService , public utility: UtilityService) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    
    return from(this.callToken()).pipe( switchMap( value => {
      let cloneRequest = this.addSecret(req, value);
      return next.handle(cloneRequest)
    }))
    
    
    
      

    

  }

  

  callToken(){
      return new Promise( resolve => {
        this.sqlite.getCurrentUserAuthorizationToken().then( v => {
          resolve(v);
        }).catch( err => resolve(null));
      })

    //return this.storage.get('token');
  }

  private addSecret(request: HttpRequest<any>, value: any){

      let v = value ? value : '';
      let clone = request.clone({
        setHeaders : {
          Authorization: 'Bearer ' + v,
          Accept: "application/json",
          'Content-Type': "application/json",
        }
      })
      return clone;
  }

}
