import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { FCM } from '@ionic-native/fcm/ngx';
import { Events, Platform } from '@ionic/angular';
import { AudioService } from './audio.service';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FcmService {

  constructor(private fcm: FCM,
    private audio: AudioService,
    private sqlite: SqliteService,
    public events: Events,
    public router: Router,
    public network: NetworkService,
    public utility: UtilityService,
    public platform: Platform,

    ) {
      this.audio.preload();
      this.assignEvents();
  }

  private isDevice() {
    return this.platform.is('cordova')
  }

  assignEvents(){
    this.events.subscribe('user:shownotificationalert', this.notificationReceivedalert.bind(this));
  }

  setupFMC(){

    //Notifications
    this.fcm.subscribeToTopic('all');
    //this.getFCMToken()
    this.fcm.onNotification().subscribe( data => {

      // this.badge.increase(1);
      if(data.wasTapped){
        console.log("Received in background");
      } else {
        console.log("Received in foreground");
        this.audio.play("");
        if(data["showalert"] != null){
          this.events.publish('user:shownotificationalert', data);
        }else{
          this.events.publish('user:shownotification', data);
        }
      };
    })
    this.fcm.onTokenRefresh().subscribe(token=>{
      this.sqlite.setFcmToken(token);
      this.events.publish('user:settokentoserver');
    });
    //end notifications.

  }

  async getFCMToken(){
    return new Promise(resolve => {
      if(this.isDevice()){
        this.fcm.getToken().then(token=>{
          resolve(token);
        })
      }else{
        resolve(null);
      }
      
    });
  }

  async setTokenToServer(){
    const fcm_token = await this.getFCMToken();
    this.network.saveFcmToken({token: fcm_token}).subscribe(dats => {
    }, err => {  });
  }

  notificationReceivedalert(data){
    if(data.hasOwnProperty('showalert')){
      this.utility.showAlert(data["showalert"]).then( () => {
        if(data['showalert'] == 'Pass scanned successfully'){
          this.router.navigate(['dashboard'])
        }
      });
    }



  }


}
