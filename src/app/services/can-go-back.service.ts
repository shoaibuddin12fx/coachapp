import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CanGoBackService {

  private previousUrl: string;
  private currentUrl: string;
  private fullurl;

  constructor(private router: Router) {
    this.currentUrl = this.router.url;
    this.fullurl = window.location.href.replace('http://', '');
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {        
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
        this.fullurl = window.location.href.replace('http://', '');
        
      };
    });
  }

  public getPreviousUrl() {
    return this.previousUrl;
  }  
  
  public getCurrentUrl() {
    return this.currentUrl;
  }  

  public getFullUrl(){
    return this.fullurl;
  }
}
