import { NetworkService } from './../services/network.service';
import { UserService } from './../services/user.service';
import { Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Events, IonRefresher, PopoverController, NavController, ModalController } from '@ionic/angular';
import { ContactsService } from '../services/contacts.service';
import { PopoverComponent } from '../components/popover/popover.component';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { UtilityService } from '../services/utility.service';
import { SqliteService } from '../services/sqlite.service';
import { PassesService } from '../services/passes.service';



export abstract class BasePage {

    public isErrorViewVisible: boolean;
    public isEmptyViewVisible: boolean;
    public isContentViewVisible: boolean;
    public isLoadingViewVisible: boolean;

    protected router: Router;
    protected events: Events;
    protected userService: UserService;
    public passService: PassesService;
    public contacts: ContactsService;
    public network: NetworkService;
    public utility: UtilityService;
    private activatedRoute: ActivatedRoute;
    protected refresher: IonRefresher;
    public popoverCtrl: PopoverController;
    public sqlite: SqliteService;
    public nav: NavController;
    public keyboard: Keyboard;
    

    constructor(injector: Injector) {
        this.router = injector.get(Router);
        this.activatedRoute = injector.get(ActivatedRoute);
        this.events = injector.get(Events);
        this.userService = injector.get(UserService);
        this.passService = injector.get(PassesService);
        this.utility = injector.get(UtilityService);
        this.network = injector.get(NetworkService);
        this.contacts = injector.get(ContactsService);
        this.popoverCtrl = injector.get(PopoverController);
        this.sqlite = injector.get(SqliteService);
        this.nav = injector.get(NavController);
        this.keyboard = injector.get(Keyboard);

    }

    async presentPopover(ev: any, props) {

        const popover = await this.popoverCtrl.create({
          component: PopoverComponent,
          event: ev,
          translucent: true,
          componentProps: props,
          cssClass: 'popover-bottom'
        });
        await popover.present();
        return await popover.onDidDismiss();
        
    }

    getQueryParams() {
        return this.activatedRoute.snapshot.queryParams;
    }

    navigateTo(page: any, queryParams: any = {}) {
        return this.router.navigate([page], { queryParams });
    }
}