import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddGroupPage } from './add-group.page';
import { CustomHeaderComponentsModule } from 'src/app/components/custom-header/custom-header-component.module';

const routes: Routes = [
  {
    path: '',
    component: AddGroupPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CustomHeaderComponentsModule
  ],
  declarations: [AddGroupPage]
})
export class AddGroupPageModule {}
