import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CreatePassPage } from './create-pass.page';
import { CustomHeaderComponentsModule } from 'src/app/components/custom-header/custom-header-component.module';
import { OtherEventsComponent } from 'src/app/components/other-events/other-events.component';
import { OtherEventsComponentsModule } from 'src/app/components/other-events/otherevents-component.module';
import { SyncContactsPage } from '../sync-contacts/sync-contacts.page';
import { SyncContactsPageModule } from '../sync-contacts/sync-contacts.module';
import { ContactselectionComponentsModule } from 'src/app/components/contact-selection/contactselection-component.module';
import { ContactSelectionComponent } from 'src/app/components/contact-selection/contact-selection.component';
import { ContactitemComponentsModule } from 'src/app/components/contact-item/contactitem-component.module';
import { GoogleMapComponent } from 'src/app/components/google-map/google-map.component';
import { GoogleMapComponentsModule } from 'src/app/components/google-map/googlemap-component.module';

const routes: Routes = [
  {
    path: '',
    component: CreatePassPage
  }
];

@NgModule({
  entryComponents: [ OtherEventsComponent, ContactSelectionComponent, GoogleMapComponent ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ContactitemComponentsModule,
    CustomHeaderComponentsModule,
    OtherEventsComponentsModule,
    ContactselectionComponentsModule,
    GoogleMapComponentsModule
    
  ],
  declarations: [CreatePassPage]
})
export class CreatePassPageModule {}
