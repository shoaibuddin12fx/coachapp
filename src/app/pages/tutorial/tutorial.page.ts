import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage extends BasePage {

  @ViewChild(IonSlides) slides: IonSlides;
  slideOpts = [];
  showSkip = true;
  assetlink = 'assets/imgs/';

  slideData: any = [
    {
      heading: 'Welcome to',
      headingbold: 'COACH APP CLUB',
      image: 'tutorial.png',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    },
    {
      heading: 'Contacts, Ecercises,',
      headingbold: 'and Monitoring',
      image: 'tutorial-2.png',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    },
    {
      heading: 'Highly Secure and',
      headingbold: 'Convenient User Verification',
      image: 'tutorial-3.png',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    },
    {
      heading: 'Real-time Help',
      headingbold: 'Coach Push Notifications',
      image: 'tutorial-4.png',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    }
  ]

  constructor(injector: Injector) {
    super(injector);
  }

  async goToNextSlide(){
    const isEnd = await this.slides.isEnd();
    if(isEnd){
        this.signup()
    }else{
      this.slides.slideNext();
    }
  }

  onSlideChangeStart(slider) {
      this.showSkip = !slider.isEnd();
  }

  openLoginPage(){
    this.navigateTo('login');
  }

  signup(){
    this.navigateTo('login', {formtype: 'signup'});
  }

}
