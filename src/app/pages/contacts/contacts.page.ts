import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage extends BasePage implements OnInit {

  contact_type = 'contacts';
  filter = false;
  phone_contacts: any[] = [];
  _phone_contacts: any[] = [];
  group_contacts: any[] = [];
  _group_contacts: any[] = [];
  fav_contacts: any[] = [];
  _fav_contacts: any[] = [];


  constructor(injector: Injector) {
    super(injector);
    // this.initializeContacts();
    // this.events.subscribe('contacts:reloadcontacts', this.initializeContacts.bind(this) )
    // this.events.subscribe('contacts:reloadfav', this.switchSagment.bind(this) )
    // this.events.subscribe('contacts:reloadgroups', this.initializeGroups.bind(this) )

  }

  ngOnInit() {
    
  }
  
  async initializeContacts(){
    await this.contacts.getContactsData();
    this.switchSagment('contacts')
  }

  async initializeGroups(){
    await this.contacts.getGroupsData();
    this.switchSagment('groups')
  }

  async switchSagment(contact_type){
    console.log(contact_type);
    this.events.publish('filteralpha:clear')
    switch (contact_type){
      case 'contacts':
        this.setData(contact_type, this.contacts.getContacts())
        break;
      case 'groups':
        this.setData(contact_type, this.contacts.getGroups())
        break;
      case 'favorites':
        this.setData(contact_type, this.contacts.getFavorites())
        break;
    }
  }

  setData(type, data){
    switch (type){
      case 'contacts':
        this.phone_contacts = data;
        this._phone_contacts = data;
        break;
      case 'groups':
        this.group_contacts = data;
        this._group_contacts = data;
        break;
      case 'favorites':
        this.fav_contacts = data;
        this._fav_contacts = data;
        break;
    }
  }



  filterByAlpha($event){
    const alpha = $event.alpha;
    switch (this.contact_type){
      case 'contacts':

        this.phone_contacts = this._phone_contacts.filter((item) => {
          return (!alpha) ? true : item.display_name.toLowerCase().charAt(0) == alpha.toLowerCase();
        });
        break;
      case 'groups':
        
        break;
      case 'favorites':
        
        break;
    }

  }

  contactPopover($event, item, flag) {

    this.presentPopover(null, {pid: item, flag: flag}).then( v => {
      if (v == null || v['data'] == null) { return; }
      
      switch(flag){ 
        case 'C':
          this.contactsSwitch(v['data'])
          break;
        case 'G':
            this.groupSwitch(v['data'])
          break;
        case 'F':
          this.favoritesSwitch(v['data'])
          break; 
      }

    })

  }

  contactsSwitch(data){
    console.log(data, data["param"])
    var item = data["pid"];
    switch (data["param"]) {
      case "C":
        this.callMe(item.phone_number);
        break;
      case "G":
        this.createSelectedInContactGroupByItem([item]);
        break;
      case "E":
        this.editFamily(item.user_id);
        break;
      case "F":
        this.createSelectedInContactFavouritiesByItem([item]);
        break;
      case "D":
        this.deleteSelectedContact(item)
        break;
      case "SP":
        //this.fetchEventsToSendPassTo(item);
        break;
      case "RP":
        //this.sendPassRequestToContact(item);
        break;
    }
  }

  groupSwitch(data){
    console.log(data, data["param"])
    var item = data["pid"];
    switch (data["param"]) {
      case "S":
        item.open = !item.open;
        break;
      case "G":
        // this.addContactsToSelectedGroupByItem(item);
        break;
      case "SP":
        // this.fetchEventsToSendPassToGroup(item);
        break;
      case "E":
        this.editContactGroup(item.id);
        break;
      case "D":
        this.deleteGroup(item);
        // this.spliceItem(item);
        // this.network.deleteContactGroup(item.id).subscribe((res: any) => {
        //   console.log(res);
        //   this.utilityProvider.presentSuccessToast(
        //     res['message']
        //   );

        //   this.getMyContacts();
        // });
        break;
    }
  }

  favoritesSwitch(data){
    console.log(data, data["param"])
    var item = data["pid"];
    switch (data["param"]) {
      case "R":
        this.removeFromFavorites(item)
        break;
      case "G":
        //this.createSelectedInContactGroupByItem([item]);
        break;
      case "E":
        //this.editFamily(item.user_id);
        break;
      case "C":
        //this.callMe(item.phone_number);
        break;
      case "D":
        // item.selected = true;
        // this.removeFromFavorites(item);
        break;
      case "SP":
        //this.fetchEventsToSendPassTo(item);
        break;
      case "RP":
        //this.sendPassRequestToContact(item);
        break;
    }
  }

  callMe(num) {
    this.utility.dialMyPhone(num);
  }

  async createSelectedInContactGroupByItem(items) {
    var fi = items;
    var self = this;
    console.log(fi);

    if (fi.length <= 0) {
      return;
    }

    this.contacts.getSelectedGroupAlert().then( async v => {
      console.log(v);
      await self.contacts.addContactsToGroup(v, fi);
      this.switchSagment('groups')
    })

    
  }

  async createSelectedInContactFavouritiesByItem(items){
    const fav = await this.contacts.createSelectedInContactFavouritiesByItem(items) as [];
    this.fav_contacts = fav;
  };

  async deleteSelectedContact(item){

    await this.contacts.deleteSelectedContact(item) as number;
    const index = this.phone_contacts.findIndex(x => x.id == item.id);
    this.phone_contacts.splice(index, 1);
    this.phone_contacts = [...this.phone_contacts];
  
  }

  async deleteGroup(group){
    await this.contacts.deleteContactGroup(group);
    const index = this.group_contacts.findIndex(x => x.id == group.id);
    this.group_contacts.splice(index, 1);
    this.group_contacts = [...this.group_contacts];
  }

  async removeFromFavorites(item){
    await this.contacts.removeFromFavorites(item);
    const index = this.fav_contacts.findIndex(x => x.id == item.id);
    this.fav_contacts.splice(index, 1);
    this.fav_contacts = [...this.fav_contacts];
  }

  editFamily(c) {
    let data = { user_id: c, show_relation: false };
    this.navigateTo('/tabs/contacts/add-contact', data);
  }

  plusHeaderButton(param) {
    switch (this.contact_type) {
      case "contacts":
        if (param == "S") {
          this.editFamily(null);
        } else {
          this.syncMyContact();
        }

        break;
      case "groups":
        if (param == "AG") {
          // sync contact to and added to selected group
          // this.syncContactsToGroup();
        } else {
          this.createContactGroup();
        }

        break;
      case "favorites":
        this.addContactInFavorites();
        break;
    }
  }

  async syncMyContact() {
    this.navigateTo('/tabs/contacts/sync-contacts', {fromContacts: false});
  }

  createContactGroup() {
    this.navigateTo('/tabs/contacts/add-group');
  }

  editContactGroup(id) {
    this.navigateTo('/tabs/contacts/add-group', {id : id});
  }

  async removeContactFromGroup(groupindex, item) {
    
    const flag = await this.utility.promptAlert('Confirm Delete', 'Do you want to delete this contact from group?')
    if(!flag) { return }

    await this.contacts.removeFromGroup(item);
    this.switchSagment('groups')
    
  }

  async addContactInFavorites(){
    this.navigateTo('/tabs/contacts/sync-contacts', {fromContacts: true, favorites: true});
  }



}
