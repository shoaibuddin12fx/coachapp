import { ModalController } from '@ionic/angular';
import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../../base-page';
import { AddContactComponent } from 'src/app/components/add-contact/add-contact.component';
import { ContactSelectionComponent } from 'src/app/components/contact-selection/contact-selection.component';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage extends BasePage implements OnInit {

  phone_contacts: any[] = [];
  _phone_contacts: any[] = [];

  constructor(injector: Injector, public modalCtrl: ModalController) {
    super(injector);
  }

  ngOnInit() {
    this.initializeContacts();
  }

  async initializeContacts(){
    const data = await this.contacts.getContactsData() as [];
    this.phone_contacts = data;
    this._phone_contacts = data;
  }

  options(item){

    this.presentPopover(null, {pid: item, flag: 'C'}).then( v => {
      if (v == null || v.data == null) { return; }
      const data = v.data;
      console.log(data, data.param)
      switch (data.param) {
        case 'C':
          this.utility.dialMyPhone(item.phone_number);
          break;
        case 'G':
          this.addToGroup(item);
          break;
        case 'E':
          this.editFamily(item.user_id);
          break;
        case 'F':
          //this.createSelectedInContactFavouritiesByItem([item]);
          break;
        case 'D':
          //this.deleteSelectedContact(item)
          break;
        case 'SP':
          //this.fetchEventsToSendPassTo(item);
          break;
        case 'RP':
          //this.sendPassRequestToContact(item);
          break;
      }
      

    });

  }

  async addToGroup(item) {
    this.contacts.getSelectedGroupAlert().then( async v => {
      if(v != null){
        await this.contacts.addContactsToGroup(v, [item]);
      }
    })
  }

  async editFamily(id) {
    
    const modal = await this.modalCtrl.create({
      component: AddContactComponent,
      componentProps:{
        user_id : id,
        show_relation: false
      }
    });
    modal.onDidDismiss().then( data => {
      console.log(data);
      if(data['data']){
        this.initializeContacts();
      }
      
    })
    return await modal.present();
  }

  async syncMyContact() {
    const modal = await this.modalCtrl.create({
      component: ContactSelectionComponent,
      componentProps:{
        fromContacts : false,
      }
    });
    modal.onDidDismiss().then( async data => {
      console.log(data);
      const selectedContacts = data['data'];
      if(selectedContacts.length > 0){
        var formdata = { data: selectedContacts }
        await this.contacts.syncContacts(formdata);
        this.initializeContacts();
      }
      
    })
    return await modal.present();
  }

  plusHeaderButton(param) {
    if (param == "S") {
      this.editFamily(null);
    } else {
      this.syncMyContact();
    }
  }

}
