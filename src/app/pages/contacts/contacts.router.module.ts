import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactsPage } from './contacts.page';

const routes: Routes = [
  {
    path: '',
    component: ContactsPage,
    children: [
      {
        path: 'contacts',
        children: [
          {
            path: '',
            loadChildren: '../contacts/contacts/contacts.module#ContactsPageModule'
          }
        ],
      },
      {
        path: 'groups',
        children: [
          {
            path: '',
            loadChildren: '../contacts/groups/groups.module#GroupsPageModule'
          }
        ]
        
      },
      {
        path: 'favorites',
        children: [
          {
            path: '',
            loadChildren: '../contacts/favorites/favorites.module#FavoritesPageModule'
          }
        ]
      },

      {
        path: '',
        redirectTo: 'contacts',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'contacts',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ContactsPageRoutingModule {}
