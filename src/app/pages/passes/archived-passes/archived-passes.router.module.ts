import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArchivedPassesPage } from './archived-passes.page';


const routes: Routes = [
  {
    path: '',
    component: ArchivedPassesPage,
    children: [
      {
        path: 'archive',
        children: [
          {
            path: '',
            loadChildren: '../archived-passes/archive/archive.module#ArchivePageModule'
          }
        ],
      },
      {
        path: 'sent',
        children: [
          {
            path: '',
            loadChildren: '../archived-passes/sent/sent.module#SentPageModule'
          }
        ]
        
      },
      {
        path: 'scan',
        children: [
          {
            path: '',
            loadChildren: '../archived-passes/scan/scan.module#ScanPageModule'
          }
        ]
      },

      {
          path: '',
          redirectTo: 'archive',
          pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'archive',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ArchivedPassesPageRoutingModule {}
