import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.page.html',
  styleUrls: ['./archive.page.scss'],
})
export class ArchivePage extends BasePage implements OnInit {

  archivePasses: any[] = [];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.initArchivePasses();
  }

  async initArchivePasses() {
    this.archivePasses = await this.passService.getArchivePasses(this.userService.User.id) as [];
  }

}
