import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ArchivePage } from './archive.page';
import { PassListItemComponentsModule } from 'src/app/components/pass-list-item/pass-list-item.component.module';

const routes: Routes = [
  {
    path: '',
    component: ArchivePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    PassListItemComponentsModule
  ],
  declarations: [ArchivePage]
})
export class ArchivePageModule {}
