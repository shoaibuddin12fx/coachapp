import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SentPage } from './sent.page';
import { PassListItemComponentsModule } from 'src/app/components/pass-list-item/pass-list-item.component.module';

const routes: Routes = [
  {
    path: '',
    component: SentPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    PassListItemComponentsModule
  ],
  declarations: [SentPage]
})
export class SentPageModule {}
