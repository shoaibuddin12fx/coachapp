import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ArchivedPassesPage } from './archived-passes.page';
import { PassListItemComponentsModule } from 'src/app/components/pass-list-item/pass-list-item.component.module';
import { ScrollalphaComponentsModule } from 'src/app/components/scroll-alpha/scrollalpha-component.module';
import { ArchivedPassesPageRoutingModule } from './archived-passes.router.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArchivedPassesPageRoutingModule,
    PassListItemComponentsModule,
    ScrollalphaComponentsModule
  ],
  declarations: [ArchivedPassesPage]
})
export class ArchivedPassesPageModule {}
