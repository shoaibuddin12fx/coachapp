import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../../base-page';

@Component({
  selector: 'app-archived-passes',
  templateUrl: './archived-passes.page.html',
  styleUrls: ['./archived-passes.page.scss'],
})
export class ArchivedPassesPage extends BasePage implements OnInit {
  
  constructor(injector: Injector) {
    super(injector);
    
   }

  ngOnInit() {
  }
  
}
