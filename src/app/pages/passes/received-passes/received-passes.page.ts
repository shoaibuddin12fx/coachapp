import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../../base-page';

@Component({
  selector: 'app-received-passes',
  templateUrl: './received-passes.page.html',
  styleUrls: ['./received-passes.page.scss'],
})
export class ReceivedPassesPage extends BasePage implements OnInit {
  activePasses : any[] = [];
  constructor(injector: Injector) {
    super(injector);
    
   }

  ngOnInit() {
    this.initActivePasses();
  }

  async initActivePasses() {
    this.activePasses = await this.passService.getActivePasses(this.userService.User.id) as [];
    console.log(this.activePasses);
   }

}
