import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../../base-page';

@Component({
  selector: 'app-sent-passes',
  templateUrl: './sent-passes.page.html',
  styleUrls: ['./sent-passes.page.scss'],
})
export class SentPassesPage extends BasePage implements OnInit {
  sentPasses : any[] = [];
  constructor(injector: Injector) {
    super(injector);
    
   }

  ngOnInit() {
    this.initSentPasses();
  }
  async initSentPasses() {
    this.sentPasses = await this.passService.getSentPasses(this.userService.User.id) as [];
    console.log(this.sentPasses);
   }
}