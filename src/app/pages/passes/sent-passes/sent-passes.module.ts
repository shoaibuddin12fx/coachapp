import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SentPassesPage } from './sent-passes.page';
import { PassListItemComponentsModule } from 'src/app/components/pass-list-item/pass-list-item.component.module';
import { ScrollalphaComponentsModule } from 'src/app/components/scroll-alpha/scrollalpha-component.module';

const routes: Routes = [
  {
    path: '',
    component: SentPassesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    PassListItemComponentsModule,
    ScrollalphaComponentsModule
  ],
  declarations: [SentPassesPage]
})
export class SentPassesPageModule {}
