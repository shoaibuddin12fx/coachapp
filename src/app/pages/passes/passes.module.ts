import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PassesPage } from './passes.page';
import { CustomHeaderComponentsModule } from 'src/app/components/custom-header/custom-header-component.module';
import { PassListItemComponentsModule } from 'src/app/components/pass-list-item/pass-list-item.component.module';
import { ScrollalphaComponentsModule } from 'src/app/components/scroll-alpha/scrollalpha-component.module';
import { PassesPageRoutingModule } from './passes.router.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PassesPageRoutingModule,
    CustomHeaderComponentsModule,
    PassListItemComponentsModule,
    ScrollalphaComponentsModule,
  ],
  declarations: [PassesPage]
})
export class PassesPageModule {}
