import { EmptyviewComponentsModule } from '../../components/empty-view/emptyview-component.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DashboardPage } from './dashboard.page';
import { CustomHeaderComponentsModule } from 'src/app/components/custom-header/custom-header-component.module';
import { PassItemComponentsModule } from 'src/app/components/pass-item/passitem-component.module';
import { PassDetailComponent } from 'src/app/components/pass-detail/pass-detail.component';
import { PassDetailComponentsModule } from 'src/app/components/pass-detail/passdetail-component.module';


@NgModule({
  entryComponents: [ PassDetailComponent ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([{ path: '', component: DashboardPage }]),
    CustomHeaderComponentsModule,
    EmptyviewComponentsModule,
    PassItemComponentsModule,
    PassDetailComponentsModule
  ],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
