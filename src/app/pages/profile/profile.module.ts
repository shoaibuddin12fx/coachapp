import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfilePage } from './profile.page';
import { CustomHeaderComponentsModule } from 'src/app/components/custom-header/custom-header-component.module';
import { CountryCodeComponent } from 'src/app/components/country-code/country-code.component';
import { CountryCodeComponentsModule } from 'src/app/components/country-code/countrycode-component.module';
import { CropperComponent } from 'src/app/components/cropper/cropper.component';
import { CropperComponentsModule } from 'src/app/components/cropper/cropper-component.module';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage
  }
];

@NgModule({
  entryComponents: [ CountryCodeComponent, CropperComponent ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CustomHeaderComponentsModule,
    CountryCodeComponentsModule,
    CropperComponentsModule
  ],
  declarations: [ProfilePage]
})
export class ProfilePageModule {}
