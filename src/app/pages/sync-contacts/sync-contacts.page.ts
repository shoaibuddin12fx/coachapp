import { ModalController } from '@ionic/angular';
import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-sync-contacts',
  templateUrl: './sync-contacts.page.html',
  styleUrls: ['./sync-contacts.page.scss'],
})
export class SyncContactsPage extends BasePage implements OnInit {

  phone_contacts: any[] = [];
  buffer_contacts;
  Select = 'Select';
  toggle: boolean = false;
  fromContacts: boolean = false;
  fromFavorites: boolean = false;
  selectionIcon = 'checkbox-outline';
  

  constructor(injector: Injector, private modalCtrl: ModalController) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {

    this.fromContacts = this.utility.stringToBoolean( this.getQueryParams().fromContacts );
    if (this.fromContacts == true) {

      this.fromFavorites = this.getQueryParams().favorites
      let cn = await this.contacts.getContactsToSync() as [];
      console.log(cn);
      this.buffer_contacts = cn;
      this.phone_contacts = cn;

    } else {
        
      let cn = await this.contacts.getTempPhoneContacts() as [];
      this.buffer_contacts = cn;
      this.phone_contacts = cn;
      console.log(this.phone_contacts);
        
    }

  }

  selectAll(c){

    this.toggle = !this.toggle;
    this.selectionIcon = (this.toggle) ? 'checkbox' : 'checkbox-outline'
    for (var i = 0; i < this.phone_contacts.length; i++) {
      this.phone_contacts[i].checked = this.toggle;
    }

  }

  getItems(ev: any, flag) {
    // Reset items back to all of the items
    this.phone_contacts = this.buffer_contacts

    // set val to the value of the searchbar
    const val = ev.target.value;

    if(flag){
      this.keyboard.hide()
    }
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.phone_contacts = this.phone_contacts.filter((item) => {
        return (item.display_name != null && item.display_name.toLowerCase().indexOf(val.toLowerCase()) > -1)
        || (item.phone_number != null && item.phone_number.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  async finishSync(){

    var fi = this.phone_contacts.filter(
      itm => itm.checked == true);

    if(fi.length <= 0){
      return
    }

    if(fi.length == 1) {

      if(fi[0].phone_number == null){
        this.utility.presentToast('Phone number empty, can\'t import contact');
        return;
      }
    }

    var formdata = { data: fi }

    if(this.fromContacts == true){
      
      if(this.fromFavorites){
        await this.contacts.setContactToFav(fi);
        this.events.publish('contacts:reloadfav', 'favorites');
        this.nav.pop();
      }


    }else{

      await this.contacts.syncContacts(formdata);
      this.nav.pop();
    }


  }

}
