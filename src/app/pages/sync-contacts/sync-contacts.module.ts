import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SyncContactsPage } from './sync-contacts.page';
import { CustomHeaderComponentsModule } from 'src/app/components/custom-header/custom-header-component.module';
import { EmptyviewComponentsModule } from 'src/app/components/empty-view/emptyview-component.module';

const routes: Routes = [
  {
    path: '',
    component: SyncContactsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CustomHeaderComponentsModule,
    EmptyviewComponentsModule
  ],
  declarations: [SyncContactsPage]
})
export class SyncContactsPageModule {}
