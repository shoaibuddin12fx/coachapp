import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  
  
  { path: 'tutorial', loadChildren: './pages/tutorial/tutorial.module#TutorialPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: '', loadChildren: './pages/splash/splash.module#SplashPageModule' },
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'country-code', loadChildren: './pages/country-code/country-code.module#CountryCodePageModule' },
  { path: 'archive', loadChildren: './pages/passes/archived-passes/archive/archive.module#ArchivePageModule' },
  { path: 'sent', loadChildren: './pages/passes/archived-passes/sent/sent.module#SentPageModule' },
  { path: 'scan', loadChildren: './pages/passes/archived-passes/scan/scan.module#ScanPageModule' },
  { path: 'contacts', loadChildren: './pages/contacts/contacts/contacts.module#ContactsPageModule' },
  { path: 'groups', loadChildren: './pages/contacts/groups/groups.module#GroupsPageModule' },
  { path: 'favorites', loadChildren: './pages/contacts/favorites/favorites.module#FavoritesPageModule' },
  
  // { path: 'passes', loadChildren: './pages/passes/passes.module#PassesPageModule' },
  // { path: 'contacts', loadChildren: './pages/contacts/contacts.module#ContactsPageModule' },
  // { path: 'request-pass', loadChildren: './pages/request-pass/request-pass.module#RequestPassPageModule' },
  // { path: 'create-pass', loadChildren: './pages/create-pass/create-pass.module#CreatePassPageModule' },
  // { path: 'received-passes', loadChildren: './pages/passes/received-passes/received-passes.module#ReceivedPassesPageModule' },
  // { path: 'archived-passes', loadChildren: './pages/passes/archived-passes/archived-passes.module#ArchivedPassesPageModule' },
  // { path: 'sent-passes', loadChildren: './pages/passes/sent-passes/sent-passes.module#SentPassesPageModule' },
  // { path: 'verification-code', loadChildren: './pages/verification-code/verification-code.module#VerificationCodePageModule' },
  // { path: 'households', loadChildren: './pages/households/households.module#HouseholdsPageModule' },
  // { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  // { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  //{ path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule' }
  
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
