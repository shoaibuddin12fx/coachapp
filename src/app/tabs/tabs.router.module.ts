import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'dashboard',
        children: [
          {
            path: '',
            loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule'
          },
          {
            path: 'verification-code',
            loadChildren: '../pages/verification-code/verification-code.module#VerificationCodePageModule'
          },
          {
            path: 'households',
            loadChildren: '../pages/households/households.module#HouseholdsPageModule'
          },
          {
            path: 'settings',
            loadChildren: '../pages/settings/settings.module#SettingsPageModule'
          },
          {
            path: 'profile',
            loadChildren: '../pages/profile/profile.module#ProfilePageModule'
          },

        ]
      },
      {
        path: 'passes',
        children: [
          {
            path: '',
            loadChildren: '../pages/passes/passes.module#PassesPageModule',
          },
        ]
      },
      {
        path: 'contacts',
        children: [
          {
            path: '',
            loadChildren: '../pages/contacts/contacts.module#ContactsPageModule'
          },          
          { 
            path: 'add-group', 
            loadChildren: '../pages/add-group/add-group.module#AddGroupPageModule'
          },
          { path: 'sync-contacts', 
            loadChildren: '../pages/sync-contacts/sync-contacts.module#SyncContactsPageModule' 
          },
        ]
      },
      {
        path: 'request-pass',
        children: [
          {
            path: '',
            loadChildren: '../pages/request-pass/request-pass.module#RequestPassPageModule'
          }
        ]
      },
      {
        path: 'create-pass',
        children: [
          {
            path: '',
            loadChildren: '../pages/create-pass/create-pass.module#CreatePassPageModule'
          }
        ]
      },

      {
        path: '',
        redirectTo: '/tabs/dashboard',
        pathMatch: 'full'
      }
    ],
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/tabs/dashboard',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
