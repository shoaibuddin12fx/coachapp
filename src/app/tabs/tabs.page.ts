import { Component } from '@angular/core';
import { Events } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private router: Router, public events: Events) {
    this.subscribeEvents()
  }

  openLink(link){
    if(link == 'logout'){
      this.events.publish('user:logout');
    }else{
      this.router.navigate([link]);
    }
    
  }

  subscribeEvents(){
    this.events.subscribe('sidemenu:link', this.openLink.bind(this));
  }


}
